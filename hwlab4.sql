CREATE TABLE Dept_Locations
(
	Lnumber		INT		 NOT NULL,
	LocationName	VARCHAR(15)	 NOT NULL,
	PRIMARY KEY(Lnumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE Manager
(
	MNumber		INT		NOT NULL,
	FName		VARCHAR(30)	NOT NULL,
	LName		VARCHAR(30)	NOT NULL,
	PersonalID	CHAR(13)	NOT NULL,
	Sex		CHAR,
	Bdate		DATE,
	ComeFrom	VARCHAR(30),
	PRIMARY KEY(MNumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE Department
(
	Dnumber		INT		NOT NULL,
	Dname		VARCHAR(15)	NOT NULL,
	ManagerID	INT		NOT NULL,
	Mgr_start_date	DATE		NOT NULL,
	Location_id	INT,
	PRIMARY KEY(Dnumber),
	FOREIGN KEY(Location_id) REFERENCES Dept_Locations(Lnumber),
	FOREIGN KEY(ManagerID) REFERENCES Manager(MNumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE Employee
(
	EmpId		INT		NOT NULL,
	Fname		VARCHAR(30)	NOT NULL,
	Lname		VARCHAR(30)	NOT NULL,
	PersonalID	CHAR(13)	NOT NULL,
	Bdate		DATE,
	Address		VARCHAR(30),
	Sex		CHAR,
	Salary		DECIMAL(10,2),
	Dno		INT		NOT NULL,

	PRIMARY KEY(EmpID),
	FOREIGN KEY(Dno) REFERENCES Department(Dnumber)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

