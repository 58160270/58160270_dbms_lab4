CREATE VIEW emp AS
	SELECT Employee.Fname,Employee.Lname,Employee.Salary,Department.Dname
	FROM Employee JOIN Department
	ON Employee.EmpID = Department.Dnumber;
